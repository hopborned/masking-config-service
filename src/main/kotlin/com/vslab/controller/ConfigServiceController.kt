package com.vslab.controller

import com.vslab.dto.*
import com.vslab.model.Model
import com.vslab.model.Store
import com.vslab.repository.ModelRepository
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
class ConfigServiceController(private val modelRepository: ModelRepository) {

    @GetMapping("/models")
    fun getModels(): List<ModelInfoDto> = modelRepository.findAll().map(Model::toInfoDto)

    @GetMapping("/models/{id}")
    fun getModelById(@PathVariable id: Int): ModelDto? = modelRepository.findById(id)
        .orElse(null)?.fromEntity()

    @GetMapping("/models/{id}/stores")
    fun getStoresByModelId(@PathVariable id: Int): List<StoreDto> =
        modelRepository.findById(id).map { it.stores.map(Store::fromEntity) }.orElse(emptyList())

    @PutMapping("/model")
    fun putModel(@RequestBody model: ModelDto): Int = modelRepository.save(model.toEntity()).id!!
}

