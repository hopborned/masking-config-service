package com.vslab.dto

import com.vslab.model.Attribute
import com.vslab.model.Table

data class TableDto(
    val name: String,
    val description: String? = null,
    val isTransfer: Boolean = false,
    val isTruncate: Boolean = false,
    val selectionCriteria: String? = null,
    val attributes: List<AttributeDto>? = emptyList(),
)

fun Table.fromEntity() = TableDto(
    name,
    description,
    isTransfer,
    isTruncate,
    selectionCriteria,
    attributes.map(Attribute::fromEntity)
)

fun TableDto.toEntity() = Table(
    name = name,
    description = description,
    isTransfer = isTransfer,
    isTruncate = isTruncate,
    selectionCriteria = selectionCriteria,
    attributes = (attributes ?: mutableListOf()).map(AttributeDto::toEntity).toMutableList()
)
