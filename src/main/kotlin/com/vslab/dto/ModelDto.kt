package com.vslab.dto

import com.vslab.model.Model
import com.vslab.model.Store

open class ModelInfoDto(
    val id: Int?,
    val name: String,
    val description: String?,
)

class ModelDto(
    id: Int?,
    name: String,
    description: String?,
    val stores: List<StoreDto>
) : ModelInfoDto(id, name, description)

fun Model.fromEntity(): ModelDto {
    return ModelDto(id, name, description, stores.map(Store::fromEntity))
}

fun Model.toInfoDto(): ModelInfoDto {
    return ModelInfoDto(id, name, description)
}


fun ModelDto.toEntity(): Model {
    return Model(
        id,
        name,
        description,
        stores.map(StoreDto::toEntity).toMutableList()
    )
}

