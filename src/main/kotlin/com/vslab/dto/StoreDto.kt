package com.vslab.dto

import com.vslab.model.DbType
import com.vslab.model.Store
import com.vslab.model.Table

data class StoreDto(
    val name: String,
    val description: String?,
    val jdbcUrl: String,
    val dbType: DbType,
    val tables: List<TableDto>? = emptyList()
)

fun Store.fromEntity() = StoreDto(
    name,
    description,
    jdbcUrl,
    dbType,
    tables.map(Table::fromEntity)
)

fun StoreDto.toEntity() = Store(
    null,
    name,
    description,
    jdbcUrl,
    dbType,
    (tables ?: mutableListOf()).map(TableDto::toEntity).toMutableList()
)