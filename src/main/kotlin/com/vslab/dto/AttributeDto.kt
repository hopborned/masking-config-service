package com.vslab.dto

import com.vslab.model.Attribute


data class AttributeDto(
    val name: String,
    val description: String? = null,
    val typePersonalInformation: String
)

fun Attribute.fromEntity() = AttributeDto(
    name,
    description,
    typePersonalInformation
)

fun AttributeDto.toEntity() = Attribute(
    name = name,
    description = description,
    typePersonalInformation = typePersonalInformation
)
