package com.vslab.model

enum class DbType {
    ORACLE,
    POSTGRE,
    MYSQL,
    H2
}