package com.vslab.model

import javax.persistence.*

@Entity
@javax.persistence.Table(
    name = "STORES",
    uniqueConstraints = [UniqueConstraint(columnNames = ["MODEL_ID", "NAME"])]
)
class Store constructor(
    @Id
    @GeneratedValue
    var id: Int?,
    var name: String,
    var description: String? = null,
    var jdbcUrl: String,
    @Enumerated(EnumType.STRING)
    var dbType: DbType,
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(referencedColumnName = "ID", name = "STORE_ID")
    val tables: MutableList<Table> = mutableListOf(),
)