package com.vslab.model

import javax.persistence.*
import javax.persistence.Table

@Entity
@Table(
    name = "MODELS",
    uniqueConstraints = [UniqueConstraint(columnNames = ["NAME"])]
)
class Model constructor(
    @Id
    @GeneratedValue
    var id: Int? = null,
    var name: String,
    var description: String? = null,
    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(referencedColumnName = "ID", name = "MODEL_ID")
    val stores: MutableList<Store> = mutableListOf()

)