package com.vslab.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.UniqueConstraint

@Entity
@javax.persistence.Table(
    name = "ATTRIBUTES",
    uniqueConstraints = [UniqueConstraint(columnNames = ["TABLE_ID", "NAME"])]
)
class Attribute(
    @Id
    @GeneratedValue
    var id: Int? = null,
    var name: String,
    var description: String? = null,
    var typePersonalInformation: String
)