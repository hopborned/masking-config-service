package com.vslab.model

import javax.persistence.*

@Entity
@javax.persistence.Table(
    name = "TABLES",
    uniqueConstraints = [UniqueConstraint(columnNames = ["STORE_ID", "NAME"])]
    )
class Table(
    @Id
    @GeneratedValue
    var id: Int? = null,
    var name: String,
    var description: String? = null,
    var isTransfer: Boolean = false,
    var isTruncate: Boolean = false,
    var selectionCriteria: String? = null,
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(referencedColumnName = "ID", name = "TABLE_ID")
    val attributes: MutableList<Attribute> = mutableListOf(),
)
