package com.vslab.repository

import com.vslab.model.Model
import org.springframework.data.repository.CrudRepository

interface ModelRepository : CrudRepository<Model, Int> {

}