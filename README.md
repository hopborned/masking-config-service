## Running locally

### With gradle command line

```
./gradlew bootRun
```
OR 

### Build executable jar and start
```
./gradlew bootJar 
java -jar build/libs/config-service.jar
```

### API
```
http://localhost:8080/swagger-ui.html
```

### Database configuration
App uses a file database (H2) which created. The h2 console is automatically exposed at http://localhost:8080/h2-console
- user name: sa
- password: password
- JDBC URL: jdbc:h2:file:./config